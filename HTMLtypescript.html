<!DOCTYPE html>
<html>

<head>
  <title>HTML TypeScript - Redistributing Labor</title>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="css/interface.css">
  <link rel="stylesheet" href="css/single.css">
  <link rel="stylesheet" href="css/singleChapter.css">
  <link rel="stylesheet" href="css/htmlTypescript.css">
  <script src="js/paged.polyfill.js"></script>
  <script src="js/csstree.js"></script>
  <script src="js/addID.js"></script>
  <script src="js/figuresHandling.js"></script>
  <script src="js/shiftImages.js"></script> 
</head>

<body class="hyphenate" lang="en-us">
  <section id="comp-number-f1e71bd6-0b0b-438c-bcf5-7d71a460c9d8" class="component-front toc start-right">
    <div class="running-left">&#xA0;</div>
    <div class="running-right">&#xA0;</div>
    <header>
      <h1 class="ct">Table of Contents</h1>
    </header>
    <nav>
      <ol>
        <li class="toc-body toc-chapter"><a href="#comp-number-2d57499a-276a-4c09-ab16-49b81601a8c5"><span
              class="name">HTML&#xA0;Typescript - redistributing labor</span></a></li>
      </ol>
    </nav>
  </section>
  <section id="comp-number-2d57499a-276a-4c09-ab16-49b81601a8c5" class="component-body chapter start-right"><span
      class="restart-numbering"></span>
    <div class="running-left">HTML&#xA0;Typescript - redistributing labor</div>
    <div class="running-right">HTML&#xA0;Typescript - redistributing labor</div>
    <header><span class="chapter-number">1</span>
      <h1 class="component-title">HTML&#xA0;Typescript - redistributing labor</h1>
    </header>
    <p class="cst">A thought piece on how Single Source Publishing redistributes labor.</p>
    <p class="author">Adam Hyde and Wendell Piez, Coko Foundation, May 6, 2018</p>
    <p class="paragraph"><em>note: written in 2018 and to be updated soon!</em></p>
    <!-- <figure><img src="images/xsweet-1.png" alt></figure> -->
    <p class="paragraph">Apparently small changes in writing technologies can radicalise publishing workflows. <a
        href="https://mkirschenbaum.wordpress.com/" rel target="blank">Matthew Kirschenbaum</a> makes a good case for
      this in his book &#x201C;<a href="http://www.hup.harvard.edu/catalog.php?isbn=9780674417076" rel
        target="blank">Track Changes: A Literary History of Word Processing</a>&#x201C;.</p>
    <p class="paragraph">There are a number of cases in the book that illustrate this. I&#x2019;m about 1/3 of the way
      through and the example that astonished me was related to Issac Asimov.</p>
    <p class="paragraph">The setting is mid 1981 and Asimov has just encountered his first word processor at age 61
      (Kirschenbaum places 1981 as, pretty much, the start of the word processing revolution). Previous to this, Asimov
      had written with a typewriter. He was extremely fast and hence many errors were made that could not be corrected
      easily with a typewriter. Errors, in those days, were the problem of the copy editor who marked the manuscript up
      with corrections.</p>
    <p class="paragraph">As it happens, Asimov finds typing&#xA0;<em>faster</em> with the new electronic word processor
      and so he makes&#xA0;<em>more</em> mistakes. The copy editor then gets frustrated and in turn frustrates Asimov
      with many more clarifying queries&#xA0;about the errors made. Dr Asimov then starts correcting his own text using
      find-and-replace to correct common errors&#x2026;</p>
    <blockquote>
      <p class="paragraph">&#x201C;cold&#x201D; suddenly becomes &#x201C;could&#x201D; and no sign exists that it was
        ever anything else.</p>
    </blockquote>
    <p class="paragraph">The result is cleaner copy which elicits fewer inline queries from the copy editor. Right
      there, publishing workflows changed and labor was redistributed from the copy editor to the author.</p>
    <p class="paragraph">A migration from a typewriter to a word processor is&#xA0;<em>not</em> a small change. However,
      on a higher, more abstract, level the ability to correct content &#x2018;as you go&#x2019; is not a huge cognitive
      leap. When the technology enabled this, however, the change it effected was great.</p>
    <p class="paragraph">Small design changes can have huge consequences for publishing workflows to the point of
      redefining roles. With the introduction of the word processor came&#xA0;the ability to change copy easily and
      consequently the author assumed more of this role and the copy editor&#x2019;s role was redefined.</p>
    <p class="paragraph">I think we might have something similar in the systems we are building at the moment &#x2013;
      particularly <a href="http://editoria.pub/" rel target="blank">Editoria</a>, the platform for scholarly monograph
      production. I&#x2019;m not claiming the system will have an effect on the same scale of the introduction of word
      processors, rather that a few, apparently small, design decisions will have a large impact on the workflow of
      those using the system. This aiming to achieve, but&#xA0;<em>how things are done</em> ie. a similar redistribution
      of labor.</p>
    <p class="paragraph">In the current workflow of the University of California Press (UCP), with whom we are designing
      the system, the following steps occur:</p>
    <ol>
      <li>
        <p class="paragraph">The acquisition dept acquires the book from the author in form of a collection of Microsoft
          Word (MS Word) files in docx format. One file per chapter. These are sent to the production dept.</p>
      </li>
      <li>
        <p class="paragraph">Production editors (in the production dept) open each file on the premises of UCP where MS
          Word is installed with custom macros. These macros enable the editors to select a part of the text and apply
          custom styles (see image below).</p>
      </li>
      <li>
        <p class="paragraph">The newly styled MS Word files are then pushed through the review cycle featuring the copy
          editor and author(s).</p>
      </li>
    </ol>
    <p class="paragraph">Step 2, styling, is a long manual process. Also, it can&#xA0;<em>only occur</em> on computers
      that have these macros installed. Further, the subsequent edits by the copy editor and author(s) might revert some
      styling changes. So some of the work will need to be done again. Lastly, when the version of MS Word is updated
      then the Macros must be checked, potentially rewritten to keep pace with the new version, and re-installed on each
      machine.</p>
    <p class="paragraph">So&#x2026;the&#xA0;<em>small</em> change we are introducing we call&#xA0;<em>HTML
        Typescript</em>. It is nothing fancy, nothing new, but it is a fresh look at how we can bring offline word
      processing workflows into the browser. The critical problem being, how to do you get the MS word files provided by
      an author into a web-based production workflow? In clearer, slightly more technical, terms how do you transform a
      document from MS Word to HTML? &#xA0;Well&#x2026; many people literally say &#x201C;it can&#x2019;t be
      done.&#x201D; The criticism is not as literal as it sounds and really comes down to what you are trying to
      achieve. If you wish a one-to-one conversion of an author&#x2019;s structural intent for the production a
      correctly marked up HTML document, then automated conversion through any process is not going to achieve this. Not
      even the very complex&#xA0;processes that go from docx to structured XML (of some type) and then to HTML, which is
      the approach&#xA0;file conversion specialists prefer, will achieve this.</p>
    <p class="paragraph">This is where we think HTML Typescript solves some very interesting problems.</p>
    <p class="paragraph">HTML Typescript offers a reframing of the problem that, consequently, enables&#xA0;processes
      that closely match the publisher&#x2019;s current workflow. It also offers the potential to radically redefine
      that workflow.</p>
    <p class="paragraph">Docx is a type of XML. It is, laughably, called <a
        href="https://en.wikipedia.org/wiki/Office_Open_XML" rel target="blank">OpenXML</a>&#xA0;by Miscrosoft.
      Laughably, because while you can unzip a docx file (a docx files), you can open the&#xA0;<em>document.xml</em> and
      poke around with it (see video demo below), but it is incomprehensible. There are two critical problems that
      contribute to this:</p>
    <ol>
      <li>
        <p class="paragraph">The XML is pretty unreadable by humans. It is dense and verbose and very very messy.</p>
      </li>
      <li>
        <p class="paragraph">Due to the editing environment (MS Word) and its inability to constrain authors from using
          &#x2018;font size 12, bold&#x2019; instead of marking the text correctly as a Heading Level 2 (for example),
          the resulting styles applied by the author, and described&#xA0;in the XML, are erratic at best.</p>
      </li>
    </ol>
    <figure></figure>
    <p class="paragraph">Number 2 is the big problem, and issue number 1 helps obfuscate it.</p>
    <p class="paragraph">So, when you want to transfer from docx to HTML, it is very difficult to determine the
      author&#x2019;s strucural intent without looking at the original display version of the MS Word file. Is this
      indented single line marked &#x2018;font size 16.6, bold&#x2019; a heading 1,2, or 3&#x2026;or is it meant to be a
      block quote? For example&#x2026;</p>
    <p class="paragraph">This is exactly why the macros mentioned above must be used, because named styles are not used
      &#x2018;upstream&#x2019; by the author. So the production editors must work their way through each file and apply
      the correct designated styles using the custom macros.</p>
    <p class="paragraph">With XSLT, the conversion process of choice for file conversion pros, it is very possible to
      convert from docx to HTML. Nothing impossible there. You don&#x2019;t end up losing content (there are a few
      gotchas for this which I will cover in later posts) but you do end up with messy unstructured HTML. This is
      because the original docx is messy and unstructured. This is what people mean when they say &#x2018;it
      can&#x2019;t be done&#x2019; ie. you cannot infer all the structural intent of the author from the docx file,
      because it is a mess, and by some magic subsequently convert it to lovely clean, structured, HTML.</p>
    <p class="paragraph">But, that is actually ok. Converting to &#x2018;messy&#x2019; HTML really puts you in about the
      same position as having a messy docx file. They are both equivalently unstructured.</p>
    <p class="paragraph">File conversion specialists don&#x2019;t like this position. They want, by their very nature,
      documents to be nicely structured. HTML does not, in its raw form, stipulate much structure. Sure you can have
      headings 1,2,3 but also you can have, as MS Word does, arbitrary font sizes and styles that&#xA0;<em>look</em>
      like a heading, but the underlying markup doesn&#x2019;t explicitly state this is the case. In addition, HTML
      doesn&#x2019;t worry about document sectional structure too much. What if you wish to identify a section of the
      text as a &#x2018;method&#x2019; section? How do you deal with that? Custom XML can deal with this&#xA0;as you can
      design a mark-up structure to suit. But HTML, as it is described by the standards, doesn&#x2019;t enable this
      (yet&#x2026;<a href="https://en.wikipedia.org/wiki/Web_Compo" rel target="blank">web components</a> will change
      this to some degree).&#xA0;<em>However,</em>&#xA0;with plain ole HTML you can use any sort of div or span you like
      to wrap around sections of a HTML document and call them what you like. For example, you can have &#x2018;div
      class=&#x201D;method&#x201D;&#x2018; and there you go&#x2026;the section is defined, for&#xA0;<em>your</em>
      intents and purposes, as a method section. This does not have much currency in the outside world (the world
      outside your platform) since there is no standard way of describing a method section in HTML, but for the purposes
      of creating production systems, this is really ok. We can work with this. The important thing is that we can now
      apply document structure with HTML should we wish to. When we wish the document to leave the system we can make
      that conversion at export time (more on this below).</p>
    <p class="paragraph">So&#x2026; Where does that leave us? Ok&#x2026; well, at the time of conversion we can move
      from docx to HTML and make some &#x2018;educated&#x2019; guesses as to what the author&#x2019;s intent is. For
      example, if there is one single line text with &#x2018;font size 24, bold&#x2019; and then 16 of &#x2018;font size
      20, bold&#x2019; and 6 of &#x2018;font size 14, bold&#x2019; and then a bunch of sentences groups with font size
      12 &#x2013; then we can map these to heading 1,2,3 respectively, and the last being standard paragraphs. It
      is&#xA0;<em>not</em> perfect -it will not catch all structure, and the process will make incorrect inferences.
      However, it will get us part of the way there. So we can&#xA0;<em>already</em> start improving on the structure of
      the MS Word file automagically.</p>
    <p class="paragraph">Arguably, we are in a better position with an initial rules-based clean up of the file
      structure as it passes from docx to HTML. The good thing is that we can improve these rules over time. In time,
      the automated conversion will produce better results.</p>
    <p class="paragraph">After a conversion of&#xA0;this kind, we have a&#xA0;<em>partially</em> structured file. This
      is where file conversion specialists often leave the conversation. They don&#x2019;t like partially structured
      anything. It is not in their DNA. That is because they are primarily concerned with conforming&#xA0;file A into
      structure X. Their metric is &#x2018; is it&#xA0;well structured?&#x2019;. It is a pass/fail binary. If you are to
      look primarily at whether file A or B is well structured&#xA0;then you want well-defined schemas that
      describe&#xA0;the structure, and you want files that subscribe perfectly to that schema. Anything that falls out
      of this is a fail. Partially structured documents are a fail.</p>
    <p class="paragraph">But with the HTML Typescript approach we see&#xA0;partially structured documents as a strength,
      not a weakness.&#xA0;We know it is not possible to get to perfectly formed documents&#xA0;in one go,
      so&#xA0;rather than consider this a fail we accept we&#xA0;must get there&#xA0;<em>progressively</em>. That is the
      fundamental principle behind what we are calling <em>HTML Typescript.&#xA0;</em>It is the use of HTML as a
      document format that can be progressively improved to get to the structure you desire over time using both machine
      and manual processes.</p>
    <p class="paragraph">HTML is the perfect format for this. HTML&#x2019;s lack of formal structure, along with its
      ability to define any kind of structure you want, enables us to progressively add any kind of structure we need to
      a document. One part of this process is the automated clean up at conversion time, and the next part of the
      process is where it starts getting interesting&#x2026; this is the&#xA0;<em>manual</em> application of structure.
    </p>
    <p class="paragraph">This is where the <em>apparent</em>&#xA0;<em>weakness</em> of HTML becomes a strength &#x2013;
      we can manually add structure over time. We can progressively structure the document. For this&#xA0;process, we
      can build, and the Coko team are building, a suite of tools in the production environment so that a production
      editor (for example) can click on an element (eg a heading) and choose the style they wish to apply from a menu
      &#x2013; similar to how they currently work with macros.</p>
    <p class="paragraph">The advantages of adding the correct structure in the browser vs&#xA0;MS Word? Well, firstly,
      as mentioned above, we can computationally improve the structure before the manual process. This results in less
      work to do. Secondly, we have complete control over the tools available to the platform&#x2019;s distributed
      citizens (I don&#x2019;t like the term &#x2018;user&#x2019; and so are experimenting with other terms.
      Let&#x2019;s try citizen for now). Hence we can make the tools available to everyone, not to just those citizens
      with the MS Word macros installed. That means:</p>
    <ol>
      <li>
        <p class="paragraph">There is no need to update the (equivalent of) macros against the underlying desktop
          software version across many machines.</p>
      </li>
      <li>
        <p class="paragraph">If I wish to update the features that enable styling, then all users can leverage these
          updates immediately.</p>
      </li>
    </ol>
    <p class="paragraph">So, as a publisher, I&#x2019;m not stuck in the harrowing, expensive, cycle of continual
      software upgrades and installs against random (or planned) updates of MS Word that maybe conducted by my org. That
      is already a saving. There is a second tier of savings here &#x2013; we are building open source systems with the
      intention that they are used by a large number of orgs. If we share a common platform and common toolkit, we can
      also share the costs of maintenance and development, and free from vendor upgrade cycles. Each publisher
      doesn&#x2019;t have to do their own software development to keep their own styling macros up to date. Yet you can
      still innovate and develop new features &#x2013; while any new feature can potentially be available to everyone.
    </p>
    <p class="paragraph">But&#x2026;more importantly&#x2026;just as with great power comes great responsibility, we
      could say that <em>with a shared toolset comes shared responsibility</em>. That doesn&#x2019;t reference the
      sharing of costs mentioned above (although it could) but rather it references the roles of the people in the
      organisation using the toolset. If the tools for styling are available to all citizens, then so might the
      responsibility for using those tools. Much like Asimov noticed he could now do some of the copy editing, so might
      a publisher re-distribute the work of styling a text.&#xA0;This is where a small design decision might have a
      large impact. Publishers that are forced to design workflows based, literally, on <em>where the tools
        are</em>&#xA0;(what machines the macros are installed on in this case), can now design workflows dependent on
      who they think would be best placed to do the work. That is pretty interesting. Such a small design
      decision&#xA0;might&#xA0;actually cause pretty radical changes to workflow.</p>
    <p class="paragraph">There are a couple of things I want to comment on with regard to this. Firstly, achieving
      efficiencies like the above are only available to you if you design&#xA0;<em>systems</em>&#xA0;rather than
      software. Designing software is a technical endeavor, designing systems is not. Designing systems requires an
      understanding of what is trying to be achieved, by whom, and with what constraints. It is very difficult, for
      example, to imagine and design a progressive approach to structuring files if you are simply focused on building
      software that produces well-structured files. It sounds like a distinction without a difference, but it&#x2019;s
      not. The difference is profound.</p>
    <p class="paragraph">Secondly, what I have described above requires, in this implementation, <em>HTML
        Typescript</em>. But HTML Typescript is not a format, it is merely a way of using an existing format &#x2013;
      HTML &#x2013; and seeing its apparent weakness (no strict formal structure) as a strength. So HTML Typescript is
      not really a technical solution so much as it is an approach &#x2014; one which enables us to build interesting
      systems to solve the long-standing problem of &#x2018;getting out of MS Word&#x2019; and into the web.</p>
    <p class="paragraph">Lastly, there may be some concern that this kind of approach doesn&#x2019;t get us to
      structured formats like JATS (or whatever) that may be required by your content sector. Well, that&#x2019;s not a
      big deal. You can design your version of HTML with all the structure you need to make an easy conversion to
      whatever XML format you want. That&#x2019;s not so tricky. HTML can contain that structure and you can build the
      production tools (the web-based equivalent of the MS Word macros) to suit. In other words, end-of-line formats
      (for distribution or syndication) should remain an end-of-line problem. When you press &#x2018;export&#x2019; (or
      whatever), then that is the time, presumably, when the document is nicely structured, that you convert it to
      whatever you need in a relatively straightforward conversion.</p>
    <p class="paragraph">In summary, I&#x2019;ve been kicking around in this area for a long time and feel that there
      are some ideas that are surfacing that not only solve some tricky problems, but open the door to new
      possibilities. I think the HTML Typescript approach is one such case and I&#x2019;m looking forward to finishing
      the implementation of the HTML Typescript approach with Editoria and trialing in the weeks to come. More thoughts
      to come.</p>
    <p class="paragraph"></p>
  </section>
</body>

</html>
