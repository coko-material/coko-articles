// lets you manualy add classes to some pages elements
// to simulate page floats.
// works only for elements that are not across two pages

let classElemFloatSameTop = "imgToTop"; // ← class of floated elements on same page
let classElemFloatSameBottom = "imgToBottom"; // ← class of floated elements bottom on same page

class floatSame extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
    this.floatBottom = false;
    // this.token;
  }

  renderNode(nextClone, node) {
    // If you find a float page element, move it in the array,
    if (node.nodeType == 1) {
      if (this.floatBottom == true) {
        let closest = nextClone.closest(`.${classElemFloatSameBottom}`);

        if (closest && !node.nextSibling) {
          let closestHeight = nextClone.getBoundingClientRect().height;

          let page = document.querySelector(".pagedjs_pages").lastElementChild;

          let pageHeight = document
            .querySelector(".pagedjs_pages")
            .lastElementChild.querySelector(".pagedjs_page_content")
            .getBoundingClientRect().height;

          page
            .querySelector(".pagedjs_page_content")
            .insertAdjacentElement("afterbegin", closest);

          let imgElement = nextClone.closest("figure").querySelector("img");

          closest.style.marginTop = `calc(${pageHeight}px - (var(--baseline) * 17) - 8px)`;


          this.floatBottom = false;
        }
      }

      if (nextClone.classList.contains(classElemFloatSameBottom)) {
        // Remove the element from the flow by hiding it.
        // should we wait for the next node?'

        console.log("needs float");
        this.floatBottom = true;
      }
    }
  }
  layoutNode(node) {
    if (node.nodeType == 1) {
      if (node.classList.contains(classElemFloatSameTop)) {
        let clone = node.cloneNode(true);

        document
          .querySelector(".pagedjs_pages")
          .lastElementChild.querySelector("section")
          .insertAdjacentElement("afterbegin", clone);

        node.style.display = "none";
      }
    }
  }
  afterPageLayout(page) {
    if (document.querySelector(".imgToBottom")) {
      let imgToBottom = document.querySelector(".imgToBottom");
      document.querySelector(".imgToBottom").classList.add("floatReverse");
      // fig caption height
      let figcaption = imgToBottom.querySelector("figcaption");
      let imgHeight = imgToBottom.querySelector("img").getBoundingClientRect()
        .height;

      figcaption.style.bottom = `${imgHeight + 8}px`;
    }
  }
  onOverflow(func) {
    // console.log(func);
    if (this.floatBottom) return false;
  }



}

Paged.registerHandlers(floatSame);

async function awaitImageLoaded(image) {
  return new Promise((resolve) => {
    if (image.complete !== true) {
      image.onload = function () {
        let { width, height } = window.getComputedStyle(image);
        resolve(width, height);
      };
      image.onerror = function (e) {
        let { width, height } = window.getComputedStyle(image);
        resolve(width, height, e);
      };
    } else {
      let { width, height } = window.getComputedStyle(image);
      resolve(width, height);
    }
  });
}
