<!DOCTYPE html>
<html>

<head>
  <title>Open Source and Scholarly Publishing</title>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="css/interface.css">
  <link rel="stylesheet" href="css/single.css">
  <link rel="stylesheet" href="css/singleChapter.css">
  <script src="js/paged.polyfill.js"></script>
  <script src="js/csstree.js"></script>
  <script src="js/addID.js"></script>
</head>

<body class="hyphenate" lang="en-us">
  <section id="comp-number-b8e25a7d-a069-4a88-b404-161cfa8cc012" class="component-front toc start-right">
    <div class="running-left">&#xA0;</div>
    <div class="running-right">&#xA0;</div>
    <header>
      <h1 class="ct">Table of Contents</h1>
    </header>
    <nav>
      <ol>
        <li class="toc-body toc-chapter"><a href="#comp-number-ed915367-04b3-4f69-b355-9332eb3dfdeb"><span
              class="name">Open Source and Scholary Publishing</span></a></li>
      </ol>
    </nav>
  </section>
  <section id="comp-number-ed915367-04b3-4f69-b355-9332eb3dfdeb" class="component-body chapter start-right"><span
      class="restart-numbering"></span>
    <div class="running-left">Open Source and Scholary Publishing</div>
    <div class="running-right">Open Source and Scholary Publishing</div>
    <header><span class="chapter-number">1</span>
      <h1 class="component-title">Open Source and Scholary Publishing</h1>
    </header>
    <p class="cst">Originally published on Scholarly Kitchen.</p>
    <p class="author">Adam Hyde, Coko Foundation, Sep 6, 2018</p>
    <p class="paragraph">There are many misconceptions about open source and scholarly publishing that often overshadow
      the enormous potential it has to lead organizations to modernized, efficient workflows and to allow them to
      innovate sustainably. Let&#x2019;s take a first look at some commonly asked questions&#x2026;</p>
    <h2>What is Open Source?</h2>
    <p class="paragraph">Open source is a license, or more accurately, <a href="https://opensource.org/licenses" rel
        target="blank">a group of licenses</a>. They grant liberal rights so that anyone can access, use, and modify the
      source code at no cost. This is contrasted to proprietary software (also known as &#x2018;closed source
      software&#x2019;) where the source code is not available to reuse or modify and, generally speaking, you must
      negotiate a fee with the creators to view the code, use or request a change to the software.</p>
    <p class="paragraph">Open source as a term should not be conflated with arguments for or against open access.
      Similarly, considering software security and weighing the benefits / risks of commercial vs non-commercial or
      amateur vs professional should be independent of the choice between open or proprietary software. If you are
      getting advice about software from someone that makes these kinds of conflations (e.g., &#x201C;Open source
      doesn&#x2019;t scale&#x201D; or &#x201C;Open source is not secure&#x201D;), then you are getting bad advice.</p>
    <h2>Is Open Source an &#x2018;amateur&#x2019; or &#x2018;part time&#x2019; endeavor?</h2>
    <p class="paragraph">Open source is a license, as stated above. The license itself does not suggest any specific
      software production methodology (e.g., Agile), or who is involved in producing the software, be they amateur or
      professional. There are diverse ways to create open source tools and many open source projects are staffed by
      talented, experienced, full time professionals.</p>
    <p class="paragraph">At Coko, for example, professional publishing staff, UX and UI people, project managers,
      software developers, facilitators, production staff, XML experts, deployment experts, and others, are all involved
      in the production of the various software solutions. These are industry professionals with many years experience.
      Further, while Coko does accept contributions from our community, code is only merged if it passes QA checks.</p>
    <p class="paragraph">This is true of many open source projects we know of in the publishing sector. OJS, Janeway, <a
        href="http://hypothes.is/" rel target="blank">Hypothes.is</a>, Libero (to name a few) are all also staffed by
      experienced software and publishing professionals.</p>
    <p class="paragraph">Open source in the world of the Coko community (eLife, Hindawi, University of California Press,
      California Digital Library, European Bioinformatics Institute and others) is produced by well paid, experienced
      professionals who work on the projects full time.</p>
    <h2>Is Open Source Secure?</h2>
    <p class="paragraph">From a high level, there are two issues to consider with software security with regard to your
      content and data:</p>
    <p class="paragraph">1.Security of the software itself</p>
    <ol>
      <li>
        <p class="paragraph">Security of the hosting environment</p>
      </li>
    </ol>
    <p class="paragraph">As for item 1, open source is no more inherently insecure or secure than proprietary software.
      Remember, open source is a license, and you cannot infer how secure a software is from the license alone.</p>
    <p class="paragraph">Having said this, while proprietary vendors will say their software is secure, you have no way
      to validate this, as you cannot audit the software yourself. With open source, you have access the source code and
      can conduct your own independent review (or pay someone else to do so). From our experience, we have also found
      that most open source projects will welcome this feedback and address any security issues found. You can validate
      these fixes by looking at the source code. Again, proprietary software providers may state they have fixed
      security issues, but because you cannot see the code, there is no way to validate this and you must take their
      word for it.</p>
    <p class="paragraph">As for the second item, open source software, like any other software, if it is to be available
      online, it must be hosted in a robust, secure environment. These kinds of secure environments are equally
      available to both proprietary software and open source software products. Again, you cannot infer the quality of a
      hosting environment by looking at the license. However, an advantage of open source is that if you do not like the
      quality of the hosting provider, you can take your data and source code and go somewhere else. Such portability is
      not so easy with proprietary software if the software creators are also the sole hosting providers.</p>
    <p class="paragraph">Interestingly, many of the software <em>that makes hosting environments secure</em> (for
      hosting open or closed software) is itself open source.</p>
    <h2>Do I have to have a software team to use open source?</h2>
    <p class="paragraph">No, you don&#x2019;t. In the publishing sector, there have been suggestions that you need a
      development team in order to merely use, let alone deploy, open source solutions. This argument is made to
      (incorrectly) argue for the apparent ease of proprietary vendor offerings that sell the software and hosting in a
      single bundle as opposed to &#x2018;more difficult&#x2019; open source offerings. This is misleading, as open
      source is not only for the technically gifted.</p>
    <p class="paragraph">The important thing to remember is that services and software are two separate things. Further,
      publishing service providers can (and do) offer services on top of open source software, which eliminates any need
      for you to have an internal tech team or any tech skills yourself.</p>
    <p class="paragraph">This decoupling of software and services effectively opens an interesting new market for
      publishing. This model breaks the exclusive one-to-one (software-to-services) vendor model that is dominant in the
      publishing sector. In other words, open source is a good way to defend against monopolies and diversify markets,
      while also protecting yourself against acquisitions &#x2014; you know the kind of thing when you wake up to find
      your workflow owned by a different organization than those who owned it when you went to sleep. The beauty of open
      source is that any hosting and service provider can pick up these tools at no cost and offer them to the
      publishing sector. This opens a competitive environment where you can change vendors if you are not happy with
      them but retain your platform, and consequently, your content, data and workflow.</p>
    <p class="paragraph">Decoupled software and services offerings are already available for open source platforms like
      the Public Knowledge Project&#x2019;s Open Journal System (OJS), and is about to be true for a wide range of very
      interesting software for Journals, Books, Micropublications, etc. that are coming out of the Coko community. So it
      is not true that you need a technical team to use open source.</p>
    <h2>Isn&#x2019;t Open Source a non-commercial thing?</h2>
    <p class="paragraph">Open source is a license and, in itself, not commercial or non-commercial. However, there are
      arguments that try to align open source with the non-profit model and then further muddy the waters by conflating
      non-profit with amateur offerings. Once you know how to spot these conflations, it becomes apparent what these
      arguments are trying to achieve (spuriously conflating open source with amateurism is typically an argument of the
      closed source vendor acting defensively).</p>
    <p class="paragraph">However, since this misconception is already out there, it is worthwhile pointing out that
      while open source is neither inherently commercial nor non-commercial, open source software can and does drive
      many enormously successful commercial businesses. Take a look at WordPress, an open source blogging/content
      management system. Around 30% of the web is hosted in WordPress, it is open source, and is the revenue mechanism
      for an amazing ecosystem of small and large commercial businesses.</p>
    <h2>Open Source is only for big tech</h2>
    <p class="paragraph">Unfortunately, some believe open source to only be relevant to big tech, and that these large
      corporations use it to consolidate markets. In fact, the opposite is held true by open source experts and critics
      alike &#x2014; that open source is a useful mechanism for enabling <a
        href="https://www.forbes.com/sites/forbestechcouncil/2018/07/16/how-open-source-became-the-default-business-model-for-software/"
        rel target="blank">economically productive collaboration</a> and contributing to market diversity (as
      illustrated above).</p>
    <h2>The Benefits of Open Source</h2>
    <p class="paragraph">So, the above attempts to de-conflate many of the misconceptions out there about open source.
      But what of the value of open source? How can it help you?</p>
    <p class="paragraph">Open source licenses afford you a lot of rights, protections, and opportunities. Some of the
      most important are as follows:</p>
    <ul>
      <li>
        <p class="paragraph">Service providers can offer their own hosted solutions of open source software.
          Consequently, an ecology of commercial publishing services can develop around the software &#x2014; so you can
          shop around for the best deal. One that fits your needs, large or small.</p>
      </li>
      <li>
        <p class="paragraph">Vendors that offer services on top of open source do not have to carry the cost of actually
          producing the software, and consequently, they don&#x2019;t need to pass that cost on to you.</p>
      </li>
      <li>
        <p class="paragraph">Should you change your mind about the services vendor, you can find a better offer and take
          your data and leave.</p>
      </li>
      <li>
        <p class="paragraph">If you desire, you can audit the software before you adopt it.</p>
      </li>
      <li>
        <p class="paragraph">You can validate security fixes. Should you have a tech team, you can modify the software
          to meet your needs.</p>
      </li>
      <li>
        <p class="paragraph">Should you not have a tech team, you can shop around for a developer that meets your budget
          and change the software to your requirements.</p>
      </li>
      <li>
        <p class="paragraph">Open infrastructure lowers the market entry threshold for innovation.</p>
      </li>
      <li>
        <p class="paragraph">You won&#x2019;t wake up &#x2018;owned&#x2019; (i.e., your workflow tools can&#x2019;t be
          acquired).</p>
      </li>
    </ul>
  </section>
</body>

</html>