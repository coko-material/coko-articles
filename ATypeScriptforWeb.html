<!DOCTYPE html>
<html>

<head>
  <title>A TypeScript For Web</title>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="css/interface.css">
  <link rel="stylesheet" href="css/single.css">
  <link rel="stylesheet" href="css/singleChapter.css">
  <script src="js/paged.polyfill.js"></script>
  <script src="js/csstree.js"></script>
  <script src="js/addID.js"></script>
</head>

<body class="hyphenate" lang="en-us">
  <section id="comp-number-e9362c78-e80b-4b5c-abc7-71f2bb2b7b53" class="component-front toc start-right">
    <div class="running-left">&#xA0;</div>
    <div class="running-right">&#xA0;</div>
    <header>
      <h1 class="ct">Table of Contents</h1>
    </header>
    <nav>
      <ol>
        <li class="toc-body toc-chapter"><a href="#comp-number-98fe23ec-dadb-4d01-90c6-a2fded056818"><span
              class="name">A Typescript for the Web</span></a></li>
      </ol>
    </nav>
  </section>
  <section id="comp-number-98fe23ec-dadb-4d01-90c6-a2fded056818" class="component-body chapter start-right"><span
      class="restart-numbering"></span>
    <div class="running-left">A Typescript for the Web</div>
    <div class="running-right">A Typescript for the Web</div>
    <header><span class="chapter-number">1</span>
      <h1 class="component-title">A Typescript for the Web</h1>
    </header>
    <p class="cst">Why HTML is a better starting point than .docx</p>
    <p class="author">Wendell Piez and Adam Hyde, Coko Foundaation, Dec 5, 2016</p>
    <p class="paragraph">Originally written in 2016, small updates in 2022.</p>
    <p class="paragraph">In the days of the typewriter, a&#xA0;typescript was a typed copy of a work. The typescript
      copy was used for improving the document through the editorial process&#xA0;&#x2013;&#xA0;for reviewing,
      commenting, fact and rights checking, revision, etc.</p>
    <p class="paragraph">For many years, since the advent of desktop publishing, the hand-typed typescript has been
      replaced by Microsoft Word documents. This advanced the publishing world quite a bit. Emailing typescript in the
      form of Word documents was far easier than mailing paper copies, and making revised copies moved from being an
      arduous typing exercise to the one click &#x2018;save as&#x2019;. There are obvious efficiencies.</p>
    <p class="paragraph">But now we are in the age of networked documents. We have the opportunity to make another
      paradigmatic workflow change &#x2013; in a sense: bringing typescript&#xA0;to the browser. With this evolution, we
      can bring an end to many of the frustrations of emailing Word documents around for comment and revision &#x2013;
      while exploring wider options&#xA0;for collaboration, innovating more interesting content types, more easily
      understanding and managing a document&#x2019;s history, using automated typesetting and data exchange. All of
      these represent cost and time savings for&#xA0;the publishing process and have the potential to move the
      communication of research beyond the <a
        href="https://coko.foundation/publishing-for-reproducibility-collaborative-input-and-networked-output/" rel
        target="blank">limited paradigm of the manuscript.</a></p>
    <p class="paragraph">However, migrating typescript&#xA0;formats away from the desktop to the web has proven to be
      very difficult.&#xA0;One of the major problems&#xA0;is that while there is a growing&#xA0;move towards online
      authoring environments, many authors still start in Word, and many previous attempts&#xA0;have shown that MS Word
      is not an easy file format to convert to other formats. Fortunately, we believe, <em>if we think of MS
        Word&#xA0;as a software for &#x201C;typescript preparation,&#x201D;</em>&#xA0;that HTML is&#xA0;a viable option
      for conversion from Word&#x2019;s &#xA0;.docx format into a typescript format ready for the web.</p>
    <p class="paragraph">HTML works out well as a format for these purposes due to&#xA0;features that are more commonly
      considered weaknesses. How so? Well,&#xA0;first we must consider that, at early stages&#xA0;of a publishing
      workflow, a Word document&#xA0;will&#xA0;not have achieved its final structure, or indeed, much of any structure
      at all. Nonetheless, attempts to &#x2018;get out of Word&#x2019; have tried to jump from unstructured Word to very
      structured XML formats by:</p>
    <ol>
      <li>
        <p class="paragraph">copying over all the data in the document and</p>
      </li>
      <li>
        <p class="paragraph">interpolating structure at the same time in an attempt to &#x2018;understand&#x2019; the
          &#x201C;intent of the author&#x201D; (or a proxy) as represented by the display semantics of the document.</p>
      </li>
    </ol>
    <p class="paragraph">But if the structure does not exist in the first place, you have a problem. It is a little like
      going, as Peter Murray Rust has previously commented, <a
        href="https://blogs.ch.cam.ac.uk/pmr/2006/09/10/hamburgers-and-cows-the-cognitive-style-of-pdf/" rel
        target="blank">from Hamburger to Cow</a>.</p>
    <p class="paragraph">However, with the <a href="https://gitlab.coko.foundation/wendell/XSweet" rel
        target="blank">XSweet</a> Word-to-HTML scripts we are developing,&#xA0;we retain&#xA0;step one (copying over all
      the data) and replace step two (interpolating structure) with a process that forsakes the introduction of any
      structure in favor of carrying over whatever information from the original Word file might be useful later on,
      whether for programmatically understanding or <em>manually applying</em> document structure. The best solution, of
      course, is a little bit of both.</p>
    <p class="paragraph">Hence we:</p>
    <ol>
      <li>
        <p class="paragraph">convert the unstructured Word document into an unstructured (or partially better
          structured) HTML document; and</p>
      </li>
      <li>
        <p class="paragraph">interpret the original Word file and&#xA0;carry forward as much information as that
          original Word file contained for possible future use in interpreting the document structure &#x2013; or
          representing any features of interest &#x2013; <em>while not actually structuring the document</em>.</p>
      </li>
    </ol>
    <p class="paragraph">Interestingly, since HTML does not require you to&#xA0;enforce a strict document structure if
      you do not have it, an unstructured document flows into HTML as easily as a structured, or partially structured,
      document flows&#xA0;into it. If your aim is a well-controlled document format, such a failure to enforce
      strict&#xA0;structures is regarded as a flaw or weakness. Yet, since we do not have much or any document structure
      in the originating Word document, and our goal is to improve it &#x2013; HTML&#x2019;s flexibility becomes a
      critical feature.</p>
    <p class="paragraph">This process produces&#xA0;an intermediary carrier format &#x2014; an information-rich,
      sanitized&#xA0;HTML format that is suitable for improvement. In&#xA0;an HTML editor, we can then bring to bear
      further tools for adding structure, reviewing, commenting, revising.&#xA0;Hence, there is a kind of similarity to
      its historical predecessor &#x2013; which is why we are using the working title &#x2018;HTML typescript&#x2019;.
    </p>
    <p class="paragraph">If the decision is to improve the structure programmatically, the Coko XSweet platform comes in
      nicely. A developer can use an XSweet &#x201C;Docx extraction&#x201D; to get to HTML typescript and then write (or
      reuse) another step, in the language of their choice, to do structural&#xA0;interpolation. By separating concerns
      here, XSweet allows developers to provide structural interpolations that suit the kinds of content their
      organizations are working with, in a way that makes the most sense for them.</p>
    <p class="paragraph">When manual improvement is required, we can&#xA0;use sophisticated editors like those built
      with the <a href="http://prosemirror.net/" rel target="blank">ProseMirror</a> libraries (such as <a
        href="https://waxjs.net/" rel target="blank">Wax</a>). This editor manages strict control of&#xA0;the source and
      ensures&#xA0;that the manually applied structure&#xA0;is semantically robust. This also means that we can apply
      explicit structure to the text and co-relate that to the other output formats we require, such as JATS XML as Coko
      has done with the Wax-JATS editor.</p>
    <p class="paragraph">It is not a revolutionary new approach, rather a shift in emphasis. By&#xA0;avoiding
      over-complicating the conversion as described above, we have been able to&#xA0;make faster headway
      &#x2013;&#xA0;and to turn our focus to the tools required to support the editorial processes via a web-based
      typescript.</p>
  </section>
</body>

</html>