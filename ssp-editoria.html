<!DOCTYPE html>
<html>

<head>
  <title>Single Source Publishing and Editoria</title>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.13.0/katex.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.7.1/styles/default.min.css">
  <link rel="stylesheet" href="css/interface.css">
  <link rel="stylesheet" href="css/single.css">
  <link rel="stylesheet" href="css/singleChapter.css">
  <link rel="stylesheet" href="css/ssp-editoria.css">
  <script src="js/paged.polyfill.js"></script>
  <script src="js/addID.js"></script>
  <script src="js/csstree.js"></script>
  <script src="js/figuresHandling.js"></script>
  <script src="js/imageRatio.js"></script>
</head>

<body class="hyphenate" lang="en-us">
  <section id="comp-number-4b78aa7e-35c1-4f7b-8da1-d43233353dd0" class="component-front toc start-right">
    <div class="running-left">&#xA0;</div>
    <div class="running-right">&#xA0;</div>
    <header>
      <h1 class="ct">Table of Contents</h1>
    </header>
    <nav>
      <ol>
        <li class="toc-body toc-chapter"><a href="#comp-number-bb01c600-9645-451e-bdee-a5402a21b331"><span
              class="name">Single Source Publishing Case Study: Editoria (Books)</span></a></li>
        <li class="toc-back toc-component"><a href="#comp-number-3139c7f2-3b76-4529-80c0-5c54a98ede63"><span
              class="name">component</span></a></li>
      </ol>
    </nav>
  </section>
  <section id="comp-number-bb01c600-9645-451e-bdee-a5402a21b331" class="component-body chapter start-right"><span
      class="restart-numbering"></span>
    <div class="running-left">Single Source Publishing Case Study: Editoria (Books)</div>
    <div class="running-right">Single Source Publishing Case Study: Editoria (Books)</div>
    <header><span class="chapter-number">1</span>
      <h1 class="component-title">Single Source Publishing Case Study: Editoria (Books)</h1>
    </header>
    <p class="author">Adam Hyde</p>
    <p class="paragraph"><em>This article is a case study of a </em><a
        href="https://coko.foundation/single-source-publishing/" rel target="blank"><em>Single Source
          Publishing</em></a><em> platform for books&#x2014;</em><a href="https://coko.foundation/product-suite/" rel
        target="blank"><em>Editoria</em></a><em> (built by </em><a href="https://coko.foundation/" rel
        target="blank"><em>Coko</em></a><em>). The content is brief and by no means a complete overview of Editoria
        features. Some demos have been prepared&#x2014;when the demo icon appears in the text please click on the icon
        to see the pre-recorded demos.</em></p>
    <p class="paragraph">Editoria is a web-based print and ebook book production platform that enables concurrent
      workflows and &#x2018;push button publishing&#x2019; via Single Source Publishing (SSP) architecture.</p>
    <figure><img
        src="images/ssp-editoria-img01.jpg">
    </figure>
    <p class="paragraph">There are three core design principles:</p>
    <ul>
      <li>
        <p class="paragraph"><strong>content components</strong>&#x2014;the book is treated as a collection of distinct
          content components (chapters, copyright pages, back matter materials etc&#x2014;we will refer to these all as
          components for the rest of this article.</p>
        <figure><img
            src="images/ssp-editoria-img02.jpg">
        </figure>
      </li>
      <li>
        <p class="paragraph"><strong>single source</strong>&#x2014;all team members (authors, copy editors,
          illustrators, designers etc) share the same document source file.</p>
        <figure><img
            src="images/ssp-editoria-img03.jpg">
        </figure>
      </li>
      <li>
        <p class="paragraph"><strong>rule-based design</strong>&#x2014;design rules (for EPUB, PDF etc) are applied to,
          but are independent of, the shared source file format.</p>
        <figure><img
            src="images/ssp-editoria-img04.jpg">
        </figure>
      </li>
    </ul>
    <p class="paragraph">The following explores how these three design principles manifest in the three main Editoria
      user interfaces within a SSP architecture, and discusses the opportunities this presents for workflow
      optimization.</p>
    <h2>User Interfaces</h2>
    <p class="paragraph">There are three main interfaces that are shared by all team members:</p>
    <ul>
      <li>
        <p class="paragraph"><strong>The Book Builder</strong>&#x2014;used for building and managing the structure of
          the book, understanding the status of the book, and accessing all content</p>
      </li>
      <li>
        <p class="paragraph"><strong>The word processor</strong>&#x2014;used for creating and improving content in the
          book</p>
      </li>
      <li>
        <p class="paragraph"><strong>Design interfaces</strong>&#x2014;used for designing the book</p>
        <figure><img
            src="images/ssp-editoria-img05.jpg">
        </figure>
      </li>
    </ul>
    <h3>The Book Builder</h3>
    <p class="paragraph">The Book Builder is the interface the team collaboratively uses to manage the top level book
      structure, understand state, and access all content.</p>
    <p class="paragraph">From the Book Builder, each team member can:</p>
    <ul>
      <li>
        manage the book structure
      </li>
      <li>
        understand the current structure of the book at a glance
      </li>
      <li>
        understand the current state of any chapter/component in an instant
      </li>
      <li>
        understand immediately the tasks they have to perform (via the state markers)
      </li>
      <li>
        immediately access the chapter or component they need to work on
      </li>
    </ul>
    <p class="paragraph">Editoria breaks a book into individual components and displays these on the Book Builder
      grouped into front matter, body, and back matter divisions.</p>
    <figure><img
        src="images/ssp-editoria-img06.jpg">
    </figure>
    <p class="paragraph">Components can be reordered by drag and drop. Updates to the book structure are updated in real
      time so all team members see changes immediately.</p>
    <p class="paragraph">Component-level workflow states (eg &#x2018;for review&#x2019;, &#x2018;proofing&#x2019; etc)
      are also managed from the Book Builder. Each component is marked with its own state, independently of the other
      components. Users with the correct permissions can change a component state (move it from one status to another).
      A component state change is also updated in real time for all users.</p>
    <p class="paragraph">Since all components have their own individual state, workflow can be optimized by enabling
      each user to advance through the production process at their own pace, allowing for many different types of tasks
      to occur in parallel across the entire book.</p>
    <figure><img
        src="images/ssp-editoria-img07.jpg">
    </figure>
    <p class="paragraph">Components states are tied to configurable roles and permissions. These role permissions give
      very granular control per state, per chapter. It is possible, for example, to allow an author to edit a chapter at
      a particular stage of the workflow (e.g. &#x2018;author revision&#x2019;) while at the same time preventing the
      author from turning track changes off. It is also possible to enable commenting-only for specific roles when a
      chapter is in a specific stage etc.</p>
    <p class="paragraph">Workflow states and permissions give the publisher full control over how linear or concurrent
      they wish their workflow to be.</p>
    <figure><img
        src="images/ssp-editoria-img08.jpg">
    </figure>
    <p class="paragraph">From the Book Builder, a user can click &#x2018;edit&#x2019; and Editoria&#x2019;s word
      processor will open that component for content creation or editing.</p>
    <p class="paragraph">On opening a component to edit, the component becomes locked. If a component is locked, other
      team members can still view the content in read-only mode. We have found many publishers prefer component-level
      locks to simultaneous realtime editing of (for example) chapters.</p>
    <h3>Content Creation and Editing</h3>
    <p class="paragraph">Content is created/edited via a web-based word processor (a purpose built software module named
      &#x2018;Wax&#x2019; after the Greek wax tablets).</p>
    <p class="paragraph">Wax is itself very configurable and extensible via plugins. By default, Wax supports all the
      general text functions you would expect from a word processor, including track changes, math, tables, special
      character support, threaded comments, images, footnotes, etc. Wax can also integrate with third-party services
      easily (such as automated processes for structuring text, or Grammarly for proofing etc). Wax supports
      simultaneous realtime editing but it is not implemented in Editoria at present (see above).</p>
    <p class="paragraph">All images for a component are managed by Editoria&#x2019;s Asset Manager which can be accessed
      either from Wax (within a component) or from the Book Builder. The Asset Manager allows for assets (mostly images)
      to have their own workflow independent of other content. This allows the artwork team (if there is one) to work in
      parallel to other team members (copy editors, authors etc).</p>
    <figure><img
        src="images/ssp-editoria-img09.jpg">
    </figure>
    <p class="paragraph">When editing or creating text, all document semantics (headings etc) must be applied using
      &#x2018;named styles&#x2019; which means the underlying document structure is as it appears to the eye
      (WYSIWYM&#x2014;&#x2019;what you see is what you mean&#x2019;). All styles are pre-determined and set by the
      publisher. It is not possible with Wax to make a paragraph look like a heading (via applying arbitrary fonts and
      size choices within Wax) without actually using the appropriate publisher-pre-defined heading style.</p>
    <p class="paragraph">When working in a single source platform such as Editoria, ensuring the underlying syntax of
      the document matches how it appears is very important. Designers, for example, cannot apply their design if they
      do not have known and reliable document semantics. Given that authors, copy editors, proofers, designers etc all
      share the same source, the underlying &#x2018;display semantics&#x2019; must correlate to explicit, known markup
      throughout the entire production process.</p>
    <p class="paragraph">The document format is obviously an important design decision when choosing or building a word
      processor that supports the above features in a Single Source Publishing system. Editoria&#x2019;s underlying
      document storage format is HTML.</p>
    <figure><img
        src="images/ssp-editoria-img10.jpg">
    </figure>
    <p class="paragraph">HTML has many advantages as a storage format in this context but first and foremost are:</p>
    <ul>
      <li>
        HTML has a near universal set of established common document semantics AND can carry
        bespoke semantics of any publisher easily
      </li>
      <li>
        HTML can carry a lot of additional structural information via class information
      </li>
      <li>
        HTML can be progressively structured without breaking.
      </li>
    </ul>
    <p class="paragraph">In addition to the above advantages for word processing, HTML also offers book designers
      opportunities in a Single Source Publishing system.</p>
    <h3>Design Interfaces</h3>
    <p class="paragraph">In Editoria, design for EPUB and print-ready PDF is managed via editable rules that are stored
      together with various other design assets (eg fonts, JavaScript snippets) as templates.</p>
    <figure><img
        src="images/ssp-editoria-img11.jpg">
    </figure>
    <p class="paragraph">The design system combines the semantically-controlled HTML content source (produced by the
      word processor Wax) with the predefined design rules (templates) to produce EPUB and PDF.</p>
    <figure><img
        src="images/ssp-editoria-img12.jpg">
    </figure>
    <p class="paragraph">It is the role of the designer to create CSS that targets the (known) output semantics from
      Wax, together with any format-specific controls (eg running heads, widows and orphans, etc), and produce beautiful
      looking print and ebooks. Designers do this by adding bespoke classes via Wax if necessary, as well as editing the
      design templates live.</p>
    <p class="paragraph">Designers can add custom classes (inline and block) to the source via the Wax editor for design
      purposes. All custom class names are carried through unaltered in the HTML to allow ease of targeting via CSS. The
      CSS design rules for print or ebook can be edited at anytime to improve the design.</p>
    <p class="paragraph">PDF output is handled by PagedJS (although it is also possible to export to InDesign formats).
      PagedJS is a typesetting system that follows the W3C PagedMedia pagination control standards to convert CSS and
      HTML into PDF. Effectively, PagedJS extends CSS to include additional controls for designing all elements of a
      print book.</p>
    <p class="paragraph">PagedJS CSS can be edited live in the paged.js design interface.</p>
    <figure><img
        src="images/ssp-editoria-img13.png">
      <figcaption class="decoration"><em>Screenshot of PagedJS Design interface</em></figcaption>
    </figure>
    <p class="paragraph">Rendering of outputs (PDF/EPUB) can occur at any time in the workflow and can be done by
      anyone, permissions pending. This means team members can see early galleys at the push of a button. It also means
      that a designer can continue designing a book and render EPUB and print-ready PDF (with the actual content in
      place) while the book is still being written.</p>
    <h2>Single Source Publishing and Editoria</h2>
    <p class="paragraph">Editoria is a complete single source publishing solution. The platform is built for small and
      large enterprises with a scalable modular microservices architecture. Almost every part of the system is
      replaceable, extensible and customisable&#x2014;allowing for an enormous scope of workflows.</p>
    <p class="paragraph">Concurrency is built into the core of the Editoria architecture. Publishers can benefit greatly
      from leveraging Editoria&#x2019;s concurrent SSP framework by optimising their workflow in innumerable ways. Some
      examples include (taken from the earlier article on SSP):</p>
    <ul>
      <li>
        Art logs and image research starting as soon as FPO (&#x2018;for placement only&#x2019;)
        images are placed
      </li>
      <li>
        Alt text being created as images are being added
      </li>
      <li>
        Indexing could start as the text is completed (inserting tags) before the proof stage
      </li>
      <li>
        Proofing being finished on one part of the book before the rest is finished
      </li>
      <li>
        Concurrent page rendering enables author to correct or fit content as they write it
      </li>
      <li>
        Testing the content with live customers as the rest of the content is being finished
        (O&#x2019;Reilly does this, and it&#x2019;s a way to do A/B testing for learning objects and assessment items
        in education. It&#x2019;s a way to improve product-market fit).
      </li>
      <li>
        Multiple authors working on the same text at the same time
      </li>
      <li>
        Designers working on design of the actual content as a work is being created
      </li>
      <li>
        Copy editors working on content immediately it is completed by an author
      </li>
      <li>
        Illustrators placing images in a work as it is being authored
      </li>
      <li>
        Production editors understanding exactly what has and hasn&#x2019;t been done at any time

      </li>
      <li>
        Developmental editors and authors working on the same text at the same time
      </li>
    </ul>
  </section>
  <section id="comp-number-3139c7f2-3b76-4529-80c0-5c54a98ede63" class="component-back component start-right">
    <div cla~ss="running-left">&#xA0;</div>
    <div class="running-right">&#xA0;</div>
    <header></header>
    <p class="paragraph"><em>Note: this article is only a partial list of Editoria features, focusing only on on some
        core system design decisions that lead to, or are a consequence of, Single Source Publishing principles.</em>
    </p>
    <p class="paragraph"><em>Thanks to Henrik van Leeuwen for illustrations and Raewyn Whyte for copy editing.</em></p>
  </section>
</body>

</html>
