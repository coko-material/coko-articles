<!DOCTYPE html>
<html>

<head>
  <title>Itch-to-Scratch Model</title>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="css/interface.css">
  <link rel="stylesheet" href="css/single.css">
  <link rel="stylesheet" href="css/singleChapter.css">
  <script src="js/paged.polyfill.js"></script>
  <script src="js/csstree.js"></script>
  <script src="js/addID.js"></script>
</head>

<body class="hyphenate" lang="en-us">
  <section id="comp-number-259adbe0-d6a4-457a-8db5-d6863612ddcc" class="component-front toc start-right">
    <div class="running-left">&#xA0;</div>
    <div class="running-right">&#xA0;</div>
    <header>
      <h1 class="ct">Table of Contents</h1>
    </header>
    <nav>
      <ol>
        <li class="toc-body toc-chapter"><a href="#comp-number-fed799a2-d465-4454-8855-f703a32bc475"><span
              class="name">How the &apos;itch-to-scratch model&apos; can solve our UX woes</span></a></li>
      </ol>
    </nav>
  </section>
  <section id="comp-number-fed799a2-d465-4454-8855-f703a32bc475" class="component-body chapter start-right"><span
      class="restart-numbering"></span>
    <div class="running-left">How the &apos;itch-to-scratch model&apos; can solve our UX woes</div>
    <div class="running-right">How the &apos;itch-to-scratch model&apos; can solve our UX woes</div>
    <header><span class="chapter-number">1</span>
      <h1 class="component-title">How the &apos;itch-to-scratch model&apos; can solve our UX woes</h1>
    </header>
    <p class="cst">Every good work of software starts by scratching a developer&apos;s personal itch. Originally published on Opensource.com</p>
    <p class="author">Adam Hyde, Coko Foundation, Apr 20, 2017</p>
    <p class="paragraph">Open source is a developer-centric solutions model, which, in a nutshell, could be described as
      building communities of developers to solve problems.</p>
    <p class="paragraph">In its most simplistic form, the model has two stages. First, a developer has a problem, which
      they can fix with some new code, and they make a start on it. Second, if they then make their solution available
      to other developers it can develop into a full blown thriving open source community. When it works it is a
      fantastic process to behold and it this simple model that has changed the history of computing.</p>
    <p class="paragraph">But outside of developers solving developer problems, open source hasn&#x2019;t done so well.
      This is pretty well discussed in open source circles, often framed in questions like: &#x201C;Why are the user
      interfaces so sucky?&#x201D; Framing the issue in terms of bad UI is common but it is perhaps a shallow treatment
      of the issue. &#x2018;User facing&#x2019; open source projects don&#x2019;t just fail at UI, they fail to meet
      user needs at a much deeper level.</p>
    <p class="paragraph">When looking around at the available open source solutions that are primarily aimed at users,
      have you ever asked yourself why open source seems to perform so poorly?It&#x2019;s not just the UX of your
      favorite desktop app that feels clunky, but why isn&#x2019;t open source winning in the desktop space in general?
    </p>
    <p class="paragraph">Where is that open source desktop app everyone wants? What about web space for that matter?</p>
    <p class="paragraph">Where are the open source equivalents that kill the proprietary opposition? Why isn&#x2019;t
      open source wiping out the competition in every user sector from text editors to CRM, from webmail to social
      media?</p>
    <p class="paragraph">There are few examples.</p>
    <p class="paragraph">I would argue Unity desktop outstrips the competition, and that Mattermost and GitLab are
      better than Slack and GitHub, but I&#x2019;m not in the majority on that.</p>
    <p class="paragraph">Outside of a small handful of examples, I can&#x2019;t see any open source products killing the
      proprietary competition when it comes to fulfilling user needs. Why has open source done so well in producing
      developer-facing solutions and web and internet infrastructure, but has done so poorly in the user-facing world?
    </p>
    <p class="paragraph">I think open source can be dominant in social networks, text editors, CRMs, webmail, and more.
      In fact, I think the open source model has some essential characteristics that lend itself to beating the
      proprietary competition right across the board. But not without first understanding why it is currently not doing
      this.</p>
    <p class="paragraph">To understand the problem, as a first step towards solving it, we must look deep inside the way
      we create solutions in open source communities. We need to do this critically and be prepared to ask ourselves
      some difficult questions.</p>
    <p class="paragraph">The itch-to-scratch model &#x201C;Every good work of software starts by scratching a
      developer&#x2019;s personal itch.&#x201D; &#x2013; Eric Raymond Eric Raymond suggested that open source and free
      software has a particular kind of solutions model he called the itch-to-scratch model. Basically, the idea is that
      open source projects start because someone, somewhere, sees a problem (the itch) and they start programming their
      way to a solution (the scratch).</p>
    <p class="paragraph">That makes perfect sense and it is a very succinct way to describe what actually happens. Eric
      Raymond wrote more about it in <a href="http://www.catb.org/~esr/writings/cathedral-bazaar/cathedral-bazaar/" rel
        target="blank">The Cathedral and the Bazaar</a>, which is considered a foundational work when it comes to
      understanding open source. As Eric Raymond states:</p>
    <blockquote>
      <p class="paragraph">&#x201C;Every good work of software starts by scratching a developer&#x2019;s personal
        itch.&#x201D; &#x2013; Eric Raymond</p>
    </blockquote>
    <p class="paragraph">The itch-to-scratch model has two very important characteristics which have played extremely
      well for open source:</p>
    <ol>
      <li>
        <p class="paragraph">The fact that it is your itch is essential for developing a good solution. If you have a
          problem and you know it very well then you are very well paced to start solving that problem. You are much
          better placed as an &#x2018;insider&#x2019; to solve the problem than an &#x2018;outsider&#x2019; that has a
          shallow grasp of the issue. Because it is your itch you are both motivated to solve it and, more importantly,
          you will have unique insights into how to solve that problem.</p>
      </li>
      <li>
        <p class="paragraph">Others with a similar problem are essential for building community. If your itch is a
          common itch, and you know others that need your partial or complete solution, then you are likely to see a
          growth in contributions and/or adoption. That&#x2019;s basically the secret sauce to the growth of open source
          projects. There is more to successful open source communities of course, like governance, volunteer
          management, understanding intrinsic and extrinsic motivations, and so on, but the heart of the matter is that
          if you know others with the same itch, open source is, if nothing else, an excellent model for magnifying the
          scratch effect. It is also not without precedent, as anyone that reads the works of John Abele or Everett
          Rogers knows. In the outside world, this process is known as &#x2018;diffusion&#x2019;.</p>
      </li>
    </ol>
    <p class="paragraph">So, the itch-to-scratch model, as a model for open source communities, really succeeds because
      you know your itch, and, secondly, you know others that share the same problem and, consequently, want to share,
      or participate in creating your solution. So far it sounds thoroughly unhygienic!</p>
    <h2>In real life</h2>
    <p class="paragraph">In the real world, we see this in action. Perhaps the cultural genesis of this is Linux. Linus
      Torvalds has a problem, he wants a free operating system (the itch). He makes a go of it (the scratch) and posts
      an email to the world asking if anyone else is interested in solving this problem with him. And, we know the rest
      of the story: one big community-driven scratch that has changed the history of computing forever. This is the
      itch-to-scratch model in action in a prototypical manner.</p>
    <p class="paragraph">That is pretty much how every successful open source project from day one has evolved.
      Developers solving technical problems and gradually attracting other developers to work with them to solve or
      adopt it. At the heart of this model are the developers. The people with both the problem and the solution. I call
      this model the developer-centric solutions model. It is, pretty much, the model for every open source project that
      exists.</p>
    <p class="paragraph">So, we know that this model is powerful. We have seen what it can do. But how many times have
      we stopped to ask ourselves what it can&#x2019;t do or what it doesn&#x2019;t do well? How often have we asked
      ourselves where it is failing and why? I don&#x2019;t think we have this conversation enough.</p>
    <p class="paragraph">Let&#x2019;s start to consider these questions by first being clear about where the
      developer-centric model works. Well, it works well when solving problems developers have, which are almost by
      definition exclusively or at least primarily, technical problems. This is why the internet is basically run on top
      of an open source layer from BIND to OpenSSH, and why you can&#x2019;t get far in the web hosting or container
      world without open source. In fact, you can&#x2019;t really develop software of any kind without using open source
      libraries of one sort or another. The developer-centric model wins, hands down when solving these kinds of
      problems.</p>
    <p class="paragraph">However, while the developer-centric model has proven to be amazingly good at solving technical
      problems, history has demonstrated that it hasn&#x2019;t proven to be very good at solving user-facing problems.
      If it were good at doing this we would expect to see open source crushing proprietary applications in the user
      space. But we don&#x2019;t see this very often.</p>
    <p class="paragraph">Could it be that this model is not good at solving these kinds of problems?</p>
    <p class="paragraph">Here is where I believe the problem lies. The developer-centric model works for solving
      technical problems because, as Eric Raymond pointed out, the people with the problem are at the heart of creating
      the solution. The developers know their itch.</p>
    <p class="paragraph">When developers are solving technical problems, problems that they have, understand well and
      want to fix, it works extremely well. And when developers are solving user problems, problems that are not their
      own, where they do not understand the issue intimately and the required solution is not primarily a technical
      issue, it does not work very well at all.</p>
    <p class="paragraph">This is because in these cases we have broken the first requirement of the itch-to-scratch
      model: know your problem. The developers do not &#x2018;know&#x2019; the problem intimately the way the users do
      because it is after all, not their problem. They are trying to solve the famous SEP, Someone Else&#x2019;s
      Problem. Consequently, they do not have the same in-depth experiential knowledge and understanding of the quirks
      and nuances of the problem that the users do. It is a qualitative, experiential, difference but it is an important
      one.</p>
    <p class="paragraph">Consequently, we get software solutions that reflect the developers understanding of the
      problem and reflects a developer&#x2019;s approach to the solution; one that typically does not match the users
      understanding of the problem or their needs.</p>
    <p class="paragraph">In open source today, developers are the dominant solutions providers. When they build
      solutions for themselves it is an almost perfect model. But when it comes to building solutions for a user base
      whose needs the developer doesn&#x2019;t intimately understand (because they are not the user), then the model
      fails.</p>
    <h2>How do we solve this?</h2>
    <p class="paragraph">I think the answer is clear. We stick to the itch-to-scratch model.</p>
    <p class="paragraph">It is a good method for solving problems. It is the model that has enabled open source to
      squash the proprietary competition in the technical world. We should stick to it, but we need to be consistent in
      how we apply it. We need to keep the people with the problem, those with the itch, the people that understand the
      problem intimately, at the heart of the problem-solving effort.</p>
    <p class="paragraph">It is not enough to add &#x2018;UX people&#x2019; or &#x2018;user forums&#x2019; to an
      otherwise developer-centric project culture to solve user needs. That approach is an interim patch and is only
      going to go so far. We need to go back to core principles of the itch-to-scratch model and bring the people with
      the problem (the users) into the heart of the solutions model.</p>
    <p class="paragraph">To do that we need to begin asking ourselves what a user-centric solutions model might look
      like for open source.</p>
    <p class="paragraph">What does it look like to you?</p>
  </section>
</body>

</html>